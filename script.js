const App = {
    data () {
        return  {
            title: "Notes",
            notes: ['note 1', 'note 2', 'note 3'],
            placeholderClick: 'type your note',
            text: ''
        }
    }, 
    methods: {
        addBtn () {
            this.notes.push(this.text)
            this.text = ''
        },
        remove(index) {
            this.notes.splice(index, 1)
        }
    } 
}
Vue.createApp(App).mount('#site')